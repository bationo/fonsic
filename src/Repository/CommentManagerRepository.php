<?php

namespace App\Repository;

use App\Entity\CommentManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CommentManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommentManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommentManager[]    findAll()
 * @method CommentManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommentManager::class);
    }

    // /**
    //  * @return CommentManager[] Returns an array of CommentManager objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommentManager
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
