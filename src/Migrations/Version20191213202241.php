<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191213202241 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mention (id INT AUTO_INCREMENT NOT NULL, mention_manager_id INT NOT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_E20259CDDB4A0274 (mention_manager_id), INDEX IDX_E20259CDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mention_manager (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mention ADD CONSTRAINT FK_E20259CDDB4A0274 FOREIGN KEY (mention_manager_id) REFERENCES mention_manager (id)');
        $this->addSql('ALTER TABLE mention ADD CONSTRAINT FK_E20259CDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article ADD mention_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66DB4A0274 FOREIGN KEY (mention_manager_id) REFERENCES mention_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E66DB4A0274 ON article (mention_manager_id)');
        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT NULL, CHANGE modified modified DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66DB4A0274');
        $this->addSql('ALTER TABLE mention DROP FOREIGN KEY FK_E20259CDDB4A0274');
        $this->addSql('DROP TABLE mention');
        $this->addSql('DROP TABLE mention_manager');
        $this->addSql('DROP INDEX UNIQ_23A0E66DB4A0274 ON article');
        $this->addSql('ALTER TABLE article DROP mention_manager_id');
        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT \'NULL\', CHANGE modified modified DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL');
    }
}
