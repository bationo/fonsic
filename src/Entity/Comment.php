<?php

namespace App\Entity;

use App\Model\CommentInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Model\MentionObjectInterface;
use App\Model\MentionManagerInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment implements CommentInterface, MentionObjectInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MentionManager", cascade={"persist", "remove"})
     */
    private $mentionManager;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CommentManager", inversedBy="comments")
     */
    private $commentManager;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }
    
    public function getCommentManager(): ?CommentManager
    {
        return $this->commentManager;
    }

    public function setCommentManager(?CommentManager $commentManager): self
    {
        $this->commentManager = $commentManager;

        return $this;
    }

    public function getMentionManager(): ?MentionManager
    {
        return $this->mentionManager;
    }

    public function setMentionManager(?MentionManagerInterface $mentionManager): self
    {
        $this->mentionManager = $mentionManager;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
