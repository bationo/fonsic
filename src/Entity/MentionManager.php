<?php

namespace App\Entity;

use App\Model\MentionInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Model\MentionManagerInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MentionManagerRepository")
 */
class MentionManager implements MentionManagerInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mention", mappedBy="mentionManager", orphanRemoval=true)
     */
    private $mentions;

    public function __construct()
    {
        $this->mentions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Mention[]
     */
    public function getMentions(): Collection
    {
        return $this->mentions;
    }

    public function addMention(MentionInterface $mention): self
    {
        if (!$this->mentions->contains($mention)) {
            $this->mentions[] = $mention;
            $mention->setMentionManager($this);
        }

        return $this;
    }

    public function removeMention(MentionInterface $mention): self
    {
        if ($this->mentions->contains($mention)) {
            $this->mentions->removeElement($mention);
            // set the owning side to null (unless already changed)
            // if ($mention->getMentionManager() === $this) {
                // $mention->setMentionManager(null);
            // }
        }

        return $this;
    }

    public function countMentions(){
        return $this->mentions->count();
    }

    public function getClassName()
    {
        return 'MentionManager';
    }
}
