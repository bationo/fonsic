<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\MentionObjectInterface;
use App\Model\CommentManagerInterface;
use App\Model\MentionManagerInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Model\CommentedObjectInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article implements MentionObjectInterface, CommentedObjectInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $libelle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Medias", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="article")
     */
    private $category;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $modified;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MentionManager", cascade={"persist", "remove"})
     */
    private $mentionManager;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CommentManager", cascade={"persist", "remove"})
     */
    private $commentManager;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->comment = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getImage(): ?Medias
    {
        return $this->image;
    }

    public function setImage(Medias $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->addArticle($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
            $category->removeArticle($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->titre ? $this->titre : 'artcile';
    }

    public function getImageWebPath(): string
    {
        return $this->getImage()->getWebPath();
    }

    public function getMentionManager(): ?MentionManager
    {
        return $this->mentionManager;
    }

    public function setMentionManager(?MentionManagerInterface $mentionManager): self
    {
        $this->mentionManager = $mentionManager;

        return $this;
    }

    public function getCommentManager(): ?CommentManagerInterface
    {
        return $this->commentManager;
    }

    public function setCommentManager(?CommentManagerInterface $commentManager): self
    {
        $this->commentManager = $commentManager;

        return $this;
    }
}
