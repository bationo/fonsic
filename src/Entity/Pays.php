<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tva;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $capital;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $monnaieCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $monnaieNom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $indicatif;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isoCode2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isoCode3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isoNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ltd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTva(): ?string
    {
        return $this->tva;
    }

    public function setTva(string $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getMonnaieCode(): ?string
    {
        return $this->monnaieCode;
    }

    public function setMonnaieCode(string $monnaieCode): self
    {
        $this->monnaieCode = $monnaieCode;

        return $this;
    }

    public function getMonnaieNom(): ?string
    {
        return $this->monnaieNom;
    }

    public function setMonnaieNom(string $monnaieNom): self
    {
        $this->monnaieNom = $monnaieNom;

        return $this;
    }

    public function getIndicatif(): ?string
    {
        return $this->indicatif;
    }

    public function setIndicatif(string $indicatif): self
    {
        $this->indicatif = $indicatif;

        return $this;
    }

    public function getIsoCode2(): ?string
    {
        return $this->isoCode2;
    }

    public function setIsoCode2(string $isoCode2): self
    {
        $this->isoCode2 = $isoCode2;

        return $this;
    }

    public function getIsoCode3(): ?string
    {
        return $this->isoCode3;
    }

    public function setIsoCode3(string $isoCode3): self
    {
        $this->isoCode3 = $isoCode3;

        return $this;
    }

    public function getIsoNumber(): ?string
    {
        return $this->isoNumber;
    }

    public function setIsoNumber(string $isoNumber): self
    {
        $this->isoNumber = $isoNumber;

        return $this;
    }

    public function getLtd(): ?string
    {
        return $this->ltd;
    }

    public function setLtd(string $ltd): self
    {
        $this->ltd = $ltd;

        return $this;
    }
}
