<?php

namespace App\Listener;

use App\Entity\Article;
use App\Entity\Comment;
use Doctrine\ORM\Events;
use App\Entity\CommentManager;
use App\Entity\MentionManager;
use Doctrine\Common\EventSubscriber;
use App\Model\MentionObjectInterface;
use App\Model\CommentedObjectInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class PersistSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // perhaps you only want to act on some "Comment" entity
        if ($entity instanceof Article) {
            $em = $args->getObjectManager();
            $entity->setCreated(new \DateTime('now'));
            $entity->setModified(new \DateTime('now'));
            $this->provideMentionManager($em, $entity);
            $this->provideCommentManager($em, $entity);
        }
        
        if($entity instanceof Comment) {
            $em = $args->getObjectManager();
            $entity->setCreated(new \DateTime('now'));
            $entity->setModified(new \DateTime('now'));
            $this->provideMentionManager($em, $entity);
            // dump($entity); die();
        }
    }

    public function provideMentionManager(ObjectManager $em, MentionObjectInterface $object)
    {
            $MentionManager = new MentionManager();
            $em->persist($MentionManager);
            $object->setMentionManager($MentionManager);
    }

    public function provideCommentManager(ObjectManager $em, CommentedObjectInterface $object)
    {
           // perhaps you only want to act on some "Comment" entity
            $commentManager = new CommentManager();
            $em->persist($commentManager);
            $object->setCommentManager($commentManager);

    }
}