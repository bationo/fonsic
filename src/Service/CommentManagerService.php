<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Mention;
use App\Form\CommentType;
use Symfony\Component\Form\Forms;
use App\Model\CommentManagerInterface;
use App\Model\MentionManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
// use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
// use Symfony\Component\HttpFoundation\Session\Session;
// use Symfony\Component\Security\Csrf\CsrfTokenManager;
// use Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use Symfony\Component\HttpFoundation\Session\Session;
// use Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage;
// use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
// use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CommentManagerService {

    private $em;
    private $session;
    private $security;
    private $request;
    // private $formFactory;

    public function setEm(EntityManagerInterface $em){
        $this->em = $em;
    }

    public function setSession(SessionInterface $session){
        $this->session = $session;
    }

    public function setSecurity(TokenStorageInterface $security){
        $this->security = $security;
    }

    public function setRequest(RequestStack $request_stack){
        $this->request = $request_stack->getCurrentRequest();
    }
    
    public function addComment(CommentManagerInterface $commentManager, Comment $comment){
        $user = $this->security->getToken()->getUser();
        if($user instanceof UserInterface){
            $comment->setUser($this->security->getToken()->getUser());
            $this->em->persist($comment);
            $commentManager->addComment($comment);
            $this->em->flush();  
        }
        else{
            throw new RuntimeException(sprintf('Pour faire un commentaire il faut se connecter.' ));
        }
    }

    public function editComment(CommentManagerInterface $commentManager, Comment $comment){
        $user = $this->security->getToken()->getUser();
        if($user instanceof UserInterface){
            if($user === $comment->getUser()){
                $this->em->flush();
                $this->session->remove('comment');
            }
            else{
                throw new RuntimeException(sprintf('Vous n\'avez pas le droit pour modifer ce commentaire.' ));
            }
        }
        else{
            throw new RuntimeException(sprintf('Pour modifer un commentaire il faut se connecter.' ));
        }
    }

    public function removeComment(CommentManagerInterface $commentManager, Comment $comment){
        $user = $this->security->getToken()->getUser();
        if($user instanceof UserInterface){
            if($user === $comment->getUser()){
                $commentManager->removeComment($comment);
                $this->em->flush(); 
            }
            else{
                throw new RuntimeException(sprintf('Vous n\'avez pas le droit pour supprimer ce commentaire.' ));
            }
        }
        else{
            throw new RuntimeException(sprintf('Pour supprimer un commentaire il faut se connecter.' ));
        }
    }

    // public function addResponse(Comment $comment, Comment $response){

    //     $this->em->persist($response);
    //     $comment->addResponse($comment);
    //     $this->em->flush();  
    // }
}