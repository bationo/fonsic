<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/front/article")
 */
class FrontArticleController extends AbstractController
{

    public static function getSubscribedServices(): array
    {
        $services = parent::getSubscribedServices();
        $services['knp_paginator']= PaginatorInterface::class;

        return $services;
    }

    /**
     * @Route("/{limit}", name="front_article_index", requirements={"limit": "[0-9]*"})
     */
    public function index(ArticleRepository $articleRepository, Request $request, $limit = 5)
    {
        $session = $this->get('session');
                $session->remove('form');
                $session->remove('comment');
        $paginator = $this->container->get('knp_paginator');
        $articles = $paginator->paginate(
            $articleRepository->findAllVisibleQuery(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            $limit /*limit per page*/
        );
        return $this->render('front/article/index.html.twig', [
            'controller_name' => 'FrontArticleController',
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/show/{id}", name="front_article_show", requirements={"id": "[0-9]*"}, methods={"GET"})
     */
    public function show(Article $article): Response
    {
        $comment = new Comment;
        $form = $this->createForm(CommentType::class, $comment);

        return $this->render('front/article/show.html.twig', [
            'article' => $article,
            'form'=>$form->createView(),
        ]);
    }
}
