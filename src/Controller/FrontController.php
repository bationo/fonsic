<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(ArticleRepository $articleRepository)
    {
        
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
            'articles' => $articleRepository->findVisibleRecent(),
        ]);
    }
}
