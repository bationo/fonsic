<?php

namespace App\Controller;

use App\Entity\Groupes;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @Route("/login", name="client_login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $status = "error";
        $message = "";
        $data = array();
        $error = null;
        $em = $this->getDoctrine()->getManager();
        $lastUsername = $authenticationUtils->getLastUsername();
        if ($request->isMethod('post')) {
            $user = $em->getRepository(User::class)->findOneByEmail($request->get('_username'));
            if ($user) {
                $bool = ($encoder->isPasswordValid($user, $request->get('_password'))) ? true : false;
                if ($bool) {
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);
                    $this->get('session')->set('_security_main', serialize($token));
                    $status = "ok";
                } else {
                    $error = "Votre  mot de passe de actuel est incorect";
                }
            } else {
                $error = "Le nom d'utilisateur est introuvable";
            }
        }
        $response = array(
            'status' => $status,
            'data' => $data,
            'message' => $error,
        );
        return new JsonResponse($response);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder): Response
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['member' => true]);
        $status = "error";
        $message = "";
        $data = array();
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $group = $em->getRepository(Groupes::class)->findOneByName('Member');
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $user->addGroupe($group);
            $em->persist($user);
            $em->flush();
            $em->refresh($user);
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $status = "ok";
        } else {
            $errors = [];
            foreach ($form as $fieldName => $formField) {
                foreach ($formField->getErrors(true) as $error) {
                    $tmp = [];
                    $tmp['libelle'] = $fieldName;
                    $tmp['val'] = $error->getMessage();
                    $data[] = $tmp;
                }
            }
            $message = "invalid form data";
        }
        $response = array(
            'status' => $status,
            'data' => $data,
            'message' => $message,
        );
        return new JsonResponse($response);
    }

    public function registerForm(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['member' => true]);
        return $this->render('front/register/form.html.twig', ['form' => $form->createView()]);
    }

}
