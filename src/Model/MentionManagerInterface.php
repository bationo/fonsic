<?php

namespace App\Model;

use App\Model\MentionInterface;


interface MentionManagerInterface
{
    public function getClassName();

    public function addMention(MentionInterface $mention);
    public function removeMention(MentionInterface $mention);
    public function getMentions();
    public function countMentions();
}
