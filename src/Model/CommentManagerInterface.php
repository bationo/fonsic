<?php

namespace App\Model;

use App\Entity\Comment;


interface CommentManagerInterface
{
    public function getClassName();

    public function addComment(Comment $comment);
    public function removeComment(Comment $comment);
    public function getComments();
    public function countComments();
}
