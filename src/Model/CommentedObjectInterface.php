<?php

namespace App\Model;

use App\Model\CommentManagerInterface;

interface CommentedObjectInterface
{
    public function setCommentManager(CommentManagerInterface $CommentManager);
    public function getCommentManager();

}
