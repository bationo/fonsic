<?php

namespace App\Model;

use App\Entity\MentionManager;
use Symfony\Component\Security\Core\User\UserInterface;

interface MentionInterface
{
    public function getMentionManager();
    public function setMentionManager(MentionManager $mentionManager);
    public function getUser();
    public function setUser(UserInterface $user);
}
