<?php

namespace App\Model;

use App\Entity\CommentManager;

interface CommentInterface
{
    public function getCommentManager();
    public function setCommentManager(CommentManager $likeManager);
}
