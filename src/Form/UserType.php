<?php

namespace App\Form;

use App\Entity\User;
use App\Form\MemberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password', PasswordType::class, ['label' => null])
            ->add('password_confirm', PasswordType::class)
            ->add('avatar', ImageType::class, array('required' => false))
        ;
        if ($options['admin']) {
            $builder->add('admin', AdminType::class, array());
        }
        if ($options['member']) {
            $builder->add('member', MemberType::class, array())
            ->remove('password_confirm')
            ->remove('avatar')
            ;
        }
        if ($options['edit']) {
            $builder->remove('password')
                ->remove('password_confirm')
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'admin' => false,
            'client' => false,
            'member' => false,
            'edit' => false,
        ]);
    }
}
